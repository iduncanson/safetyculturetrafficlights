import XCTest
import RxSwift
import RxTest

@testable import Traffic_Lights

class Traffic_Lights_ViewModelTests: XCTestCase {
    var trafficLightPhase1 : TrafficLightPhase!
    var trafficLightPhase2 : TrafficLightPhase!
    var trafficLightViewModel : TrafficLightViewModel!
    var disposeBag : DisposeBag?
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        trafficLightPhase1 = TrafficLightPhase(withName: "1", greenPhase:0.1, amberPhase:0.1)
        trafficLightPhase2 = TrafficLightPhase(withName: "2", greenPhase:0.2, amberPhase:0.2)
        trafficLightViewModel = TrafficLightViewModel(fromPhases: [trafficLightPhase1, trafficLightPhase2])
    }
    
    override func tearDown() {
        trafficLightPhase1 = nil
        trafficLightPhase2 = nil
        trafficLightViewModel = nil
        disposeBag = nil
        super.tearDown()
    }
    
    func testInitWithPhases() {
        XCTAssertEqual(trafficLightViewModel.phases.count, 2)
    }
    
    func testReinitWithPhases() {
        trafficLightViewModel.phases = [trafficLightPhase1]
        XCTAssertEqual(trafficLightViewModel.phases.count, 1)
    }
    
    func testReinitStopsRunningPhases() {
        trafficLightPhase1.start()
        trafficLightPhase2.start()
        XCTAssertTrue(trafficLightPhase1.isRunning)
        XCTAssertTrue(trafficLightPhase2.isRunning)
        
        trafficLightViewModel.phases = [trafficLightPhase1]
        XCTAssertFalse(trafficLightPhase1.isRunning)
        XCTAssertFalse(trafficLightPhase2.isRunning)
    }
    
    func testPhasesExecutedInOrder() {
        var arr = Array<(Int, TrafficLightState)>()
        var loopCount = 1
        let exp = expectation(description: "expectation")
        trafficLightPhase1.asLightStateObservable().subscribe(
            onNext: { (element) in
                arr.append((1, element))
            }
        ).addDisposableTo(disposeBag!)
        trafficLightPhase2.asLightStateObservable().subscribe(
            onNext: { (element) in
                arr.append((2, element))
                if element == .red {
                    if loopCount == 3 {
                        exp.fulfill()
                    }
                    else {
                        loopCount += 1
                    }
                }
            }
        ).addDisposableTo(disposeBag!)
        trafficLightViewModel.start()
        // Wait for the expectation to be fulfilled - in this case we actually want to timeout
        // because it should be cancelled
        waitForExpectations(timeout: 20) { error in
            // clear out observers before stopping so we don't get recursive
            self.disposeBag = nil
            self.trafficLightViewModel.stop()
            if error != nil {
                XCTFail("timed out")
            }
        }
        for index in 0..<loopCount {
            let baseIndex = index * 6
            XCTAssertTrue(arr[baseIndex + 0] == (1, .green))
            XCTAssertTrue(arr[baseIndex + 1] == (1, .amber))
            XCTAssertTrue(arr[baseIndex + 2] == (1, .red))
            XCTAssertTrue(arr[baseIndex + 3] == (2, .green))
            XCTAssertTrue(arr[baseIndex + 4] == (2, .amber))
            XCTAssertTrue(arr[baseIndex + 5] == (2, .red))
        }
    }

}

func == <T:Equatable> (tuple1:(T,T),tuple2:(T,T)) -> Bool
{
    return (tuple1.0 == tuple2.0) && (tuple1.1 == tuple2.1)
}
