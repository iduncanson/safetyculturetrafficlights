
import XCTest
import RxSwift
import RxTest

@testable import Traffic_Lights

class Traffic_Lights_ViewControllerTests: XCTestCase {
	var vc: TrafficLightViewController!
    var disposeBag : DisposeBag?
    
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        vc = storyboard.instantiateInitialViewController() as! TrafficLightViewController
        
        UIApplication.shared.keyWindow?.rootViewController = vc
        
        XCTAssertNotNil(vc.view)
    }
    
    override func tearDown() {

        disposeBag = nil
        super.tearDown()
    }
    
    func testInitialButtonState(){
        XCTAssertEqual(vc.startStopButton.currentTitle, "Start")
        XCTAssertFalse(vc.startStopButton.isSelected)
    }
    
    func testChangeButtonState() {
        XCTAssertFalse(vc.startStopButton.isSelected)
        vc.startStopButton.sendActions(for: UIControlEvents.touchUpInside)
        XCTAssertEqual(vc.startStopButton.currentTitle, "Stop")
        XCTAssertTrue(vc.startStopButton.isSelected)
    }
    
    func testChangeButtonStateStartsAnimation() {
        XCTAssertFalse(vc.startStopButton.isSelected)
        vc.startStopButton.sendActions(for: UIControlEvents.touchUpInside)
        XCTAssertTrue((vc.viewModel?.isRunning)!)
    }
    
    func testChangeButtonStateTwiceStopsAnimation() {
        XCTAssertFalse(vc.startStopButton.isSelected)
        vc.startStopButton.sendActions(for: UIControlEvents.touchUpInside)
        XCTAssertTrue((vc.viewModel?.isRunning)!)
        vc.startStopButton.sendActions(for: UIControlEvents.touchUpInside)
        XCTAssertFalse((vc.viewModel?.isRunning)!)
    }
    
    func testEmittingNewStateChangesNorthSouthImages() {
        // in this case don't start the view model because we want to toggle manually
        XCTAssertFalse((vc.viewModel?.isRunning)!)
        
        XCTAssertEqual(vc.trafficLightImageViewNW.image, UIImage(named: TrafficLightState.red.rawValue))
        XCTAssertEqual(vc.trafficLightImageViewSE.image, UIImage(named: TrafficLightState.red.rawValue))
        
        vc.northSouth.subject.onNext(.green)
        XCTAssertEqual(vc.trafficLightImageViewNW.image, UIImage(named: TrafficLightState.green.rawValue))
        XCTAssertEqual(vc.trafficLightImageViewSE.image, UIImage(named: TrafficLightState.green.rawValue))

        vc.northSouth.subject.onNext(.amber)
        XCTAssertEqual(vc.trafficLightImageViewNW.image, UIImage(named: TrafficLightState.amber.rawValue))
        XCTAssertEqual(vc.trafficLightImageViewSE.image, UIImage(named: TrafficLightState.amber.rawValue))
        
        vc.northSouth.subject.onNext(.red)
        XCTAssertEqual(vc.trafficLightImageViewNW.image, UIImage(named: TrafficLightState.red.rawValue))
        XCTAssertEqual(vc.trafficLightImageViewSE.image, UIImage(named: TrafficLightState.red.rawValue))
       
    }
    func testEmittingNewStateChangesEastWestImages() {
        // in this case don't start the view model because we want to toggle manually
        XCTAssertFalse((vc.viewModel?.isRunning)!)
        
        XCTAssertEqual(vc.trafficLightImageViewNE.image, UIImage(named: TrafficLightState.red.rawValue))
        XCTAssertEqual(vc.trafficLightImageViewSW.image, UIImage(named: TrafficLightState.red.rawValue))
        
        vc.eastWest.subject.onNext(.green)
        XCTAssertEqual(vc.trafficLightImageViewNE.image, UIImage(named: TrafficLightState.green.rawValue))
        XCTAssertEqual(vc.trafficLightImageViewSW.image, UIImage(named: TrafficLightState.green.rawValue))
        
        vc.eastWest.subject.onNext(.amber)
        XCTAssertEqual(vc.trafficLightImageViewNE.image, UIImage(named: TrafficLightState.amber.rawValue))
        XCTAssertEqual(vc.trafficLightImageViewSW.image, UIImage(named: TrafficLightState.amber.rawValue))
        
        vc.eastWest.subject.onNext(.red)
        XCTAssertEqual(vc.trafficLightImageViewNW.image, UIImage(named: TrafficLightState.red.rawValue))
        XCTAssertEqual(vc.trafficLightImageViewSE.image, UIImage(named: TrafficLightState.red.rawValue))
        
    }
    func testEmittingNewStateDoesntChangeOtherImagesNorthSouth() {
        // in this case don't start the view model because we want to toggle manually
        XCTAssertFalse((vc.viewModel?.isRunning)!)
        
        XCTAssertEqual(vc.trafficLightImageViewNW.image, UIImage(named: TrafficLightState.red.rawValue))
        XCTAssertEqual(vc.trafficLightImageViewSE.image, UIImage(named: TrafficLightState.red.rawValue))
        
        vc.eastWest.subject.onNext(.green)
        XCTAssertEqual(vc.trafficLightImageViewNW.image, UIImage(named: TrafficLightState.red.rawValue))
        XCTAssertEqual(vc.trafficLightImageViewSE.image, UIImage(named: TrafficLightState.red.rawValue))
    }
    func testEmittingNewStateDoesntChangeOtherImagesEastWest() {
        // in this case don't start the view model because we want to toggle manually
        XCTAssertFalse((vc.viewModel?.isRunning)!)
        
        XCTAssertEqual(vc.trafficLightImageViewNE.image, UIImage(named: TrafficLightState.red.rawValue))
        XCTAssertEqual(vc.trafficLightImageViewSW.image, UIImage(named: TrafficLightState.red.rawValue))
        
        vc.northSouth.subject.onNext(.green)
        XCTAssertEqual(vc.trafficLightImageViewNE.image, UIImage(named: TrafficLightState.red.rawValue))
        XCTAssertEqual(vc.trafficLightImageViewSW.image, UIImage(named: TrafficLightState.red.rawValue))
        
    }
}
