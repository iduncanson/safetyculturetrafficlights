//
//  Traffic_LightsTests.swift
//  Traffic LightsTests
//

import XCTest
import RxSwift
import RxTest

@testable import Traffic_Lights

class Traffic_Lights_PhaseTests: XCTestCase {
    var trafficLightPhase1 : TrafficLightPhase!
    var disposeBag : DisposeBag?
    override func setUp() {
        super.setUp()
        disposeBag = DisposeBag()
        trafficLightPhase1 = TrafficLightPhase(withName: "1", greenPhase:0.1, amberPhase:0.1)
    }
    
    override func tearDown() {
        trafficLightPhase1 = nil
        disposeBag = nil
        super.tearDown()
    }
    
    func testInitialization() {
        XCTAssertEqual(trafficLightPhase1.name, "1")
        XCTAssertEqual(trafficLightPhase1.greenPhase, 0.1)
        XCTAssertEqual(trafficLightPhase1.amberPhase, 0.1)
    }
    
    func testPhaseHasAllEvents() {
        let scheduler = TestScheduler(initialClock: 0)
        let exp = expectation(description: "expectation")
    
        let observer = scheduler.createObserver(TrafficLightState)
        trafficLightPhase1.asLightStateObservable().subscribe(observer).addDisposableTo(disposeBag!)
        
        trafficLightPhase1.asPhaseStateObservable().subscribe(
            onNext: { (element) in
                if element == .completed {
                    exp.fulfill()
                }
            }
        ).addDisposableTo(disposeBag!)
        
        scheduler.start()
        trafficLightPhase1.start()
        
        // Wait for the expectation to be fulfilled
        waitForExpectations(timeout: 2) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }

        XCTAssertEqual(observer.events.count, 3)
    }
    
    func testStoppdedPhaseCancelsPreviousRun() {
        let exp = expectation(description: "expectation")
        let trafficLightPhase2 = TrafficLightPhase(withName: "1", greenPhase:0.1, amberPhase:2)
        var workItem : DispatchWorkItem?
        trafficLightPhase2.asLightStateObservable().subscribe(
            onNext: { (element) in
                if element == .amber {
                    // this should be the next state work item
                    workItem = trafficLightPhase2.phaseWorkItem
                    XCTAssertNotNil(workItem)
                    trafficLightPhase2.stop()
                    XCTAssertTrue((workItem?.isCancelled)!)
                    exp.fulfill()
                }
            }
        ).addDisposableTo(disposeBag!)
        
        trafficLightPhase2.start()
        // Wait for the expectation to be fulfilled - in this case we actually want to timeout
        // because it should be cancelled
        waitForExpectations(timeout: 2)
    }
    
    func testStoppedPhaseStopsEmittingEvents() {
        let trafficLightPhase2 = TrafficLightPhase(withName: "1", greenPhase:0.1, amberPhase:0.1)
        let exp = expectation(description: "expectation")

        var state = TrafficLightState.green
        
        trafficLightPhase2.asLightStateObservable().subscribe(
            onNext: { (element) in
                if element == .green {
                    trafficLightPhase2.stop()
                }
                else if element == .amber {
                    // this is actually a bad thing - we should have been stopped before here
                    state = .amber
                    exp.fulfill()
                }
                else if element == .red {
                    state = .red
                    exp.fulfill()
                }
            }
        ).addDisposableTo(disposeBag!)

        trafficLightPhase2.start()
        
        // Wait for the expectation to be fulfilled - in this case we actually want to timeout
        // because it should be cancelled
        waitForExpectations(timeout: 2) { error in
            if error != nil || state == .amber {
                XCTFail("should not have received amber")
            }
        }
    }
    
    func testEquality() {
        XCTAssertEqual(trafficLightPhase1, trafficLightPhase1)
    }
    
    func testInequality() {
        let trafficLightPhase2 = TrafficLightPhase(withName: "2", greenPhase:0.1, amberPhase:0.1)
        XCTAssertNotEqual(trafficLightPhase1, trafficLightPhase2)
    }
}
