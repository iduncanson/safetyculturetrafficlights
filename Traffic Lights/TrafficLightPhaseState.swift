//
//  TrafficLightPhaseState.swift
//  Traffic Lights
//
//  Created by Ian Duncanson on 21/4/17.
//
//

import Foundation

enum TrafficLightPhaseState : String {
    case running
    case completed
    case stopped
}
