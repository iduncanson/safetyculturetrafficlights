import UIKit
import RxSwift
import RxCocoa

class TrafficLightViewController: UIViewController {
    @IBOutlet weak var startStopButton: UIButton!
    @IBOutlet weak var trafficLightImageViewNE: UIImageView!
    @IBOutlet weak var trafficLightImageViewSW: UIImageView!
    @IBOutlet weak var trafficLightImageViewNW: UIImageView!
    @IBOutlet weak var trafficLightImageViewSE: UIImageView!
    
    private var disposeBag = DisposeBag()
    var viewModel : TrafficLightViewModel?
    let northSouth = TrafficLightPhase(withName: "northSouth", greenPhase: 25, amberPhase: 5)
    let eastWest = TrafficLightPhase(withName: "eastWest", greenPhase: 25, amberPhase: 5)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = TrafficLightViewModel(fromPhases: [northSouth, eastWest])
        bindUI()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func bindUI() {
        startStopButton.setTitle("Start",for: .normal)
        startStopButton.setTitle("Stop",for: .selected)
        
        // pass control over to the viewmodel to start or stop the phasing animation
        startStopButton.rx.tap.asObservable().map { [weak self] (_) -> Bool in
                return !((self?.startStopButton.isSelected)!)
            }
            .do(onNext: { [weak self] (isSelected) in
                isSelected ? self?.viewModel?.start() : self?.viewModel?.stop()
            })
            .bindTo(startStopButton.rx.isSelected)
            .disposed(by: disposeBag)
        
        // observe the changes to each phase, and map them to the relevant traffic light image
        northSouth.asLightStateObservable()
            .map { name in
                UIImage(named: name.rawValue)
            }
            .subscribe(trafficLightImageViewNW.rx.image)
            .addDisposableTo(disposeBag)
        
        northSouth.asLightStateObservable()
            .map { name in
                UIImage(named: name.rawValue)
            }
            .subscribe(trafficLightImageViewSE.rx.image)
            .addDisposableTo(disposeBag)
        
        eastWest.asLightStateObservable()
            .map { name in
                UIImage(named: name.rawValue)
            }
            .subscribe(trafficLightImageViewNE.rx.image)
            .addDisposableTo(disposeBag)
        
        eastWest.asLightStateObservable()
            .map { name in
                UIImage(named: name.rawValue)
            }
            .subscribe(trafficLightImageViewSW.rx.image)
            .addDisposableTo(disposeBag)

    }
}

