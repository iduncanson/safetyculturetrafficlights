import Foundation

enum TrafficLightState : String {
    case red
    case amber
    case green
}

