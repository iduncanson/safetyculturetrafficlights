import Foundation
import RxSwift

class TrafficLightViewModel {
    var disposeBag : DisposeBag = DisposeBag()
    var currentPhase : TrafficLightPhase?
    var isRunning : Bool = false
    var phases : [TrafficLightPhase] {
        willSet {
            // we want to reset everything if we change the phasing
            stop()
        }
    }
    
    init(fromPhases phases: [TrafficLightPhase]) {
        self.phases = phases
    }
    
    func start() {
        guard !phases.isEmpty else {
            return
        }
        isRunning = true
        disposeBag = DisposeBag()
        queuePhase(phase: phases.first!)
    }
    
    func stop() {
        for phase in phases {
            phase.stop()
        }
        currentPhase = nil
        disposeBag = DisposeBag() // should empty existing observables
        isRunning = false
    }
    
    private func queuePhase(phase : TrafficLightPhase) {
        currentPhase = phase
        currentPhase?.asPhaseStateObservable().subscribe(
            onNext: { [weak self] (element) in
                guard self != nil else {
                    return
                }
                if element == .completed {
                    let phase = self?.currentPhase
                    guard phase != nil else {
                        return
                    }
                    if phase == self?.phases.last {
                        // start again at the head of the queue
                        self?.start()
                    }
                    else {
                        // move on to the next in the queue
                        let indexOf = self?.phases.index(of: phase!)
                        self?.queuePhase(phase: (self?.phases[indexOf! + 1])!)
                    }

                }
            }
        ).addDisposableTo(disposeBag)
        
        guard self.currentPhase != nil else {
            return
        }
        currentPhase?.start()
    }

}
