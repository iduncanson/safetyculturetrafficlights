import Foundation
import RxSwift

class TrafficLightPhase : Equatable {
    var greenPhase : Double = 60.0
    var amberPhase : Double = 10.0
    var isRunning : Bool = false
    var phaseWorkItem : DispatchWorkItem?
    let name : String
    let subject = PublishSubject<TrafficLightState>()
    let subjectPhase = PublishSubject<TrafficLightPhaseState>()
    
    init(withName: String, greenPhase: Double, amberPhase: Double) {
        name = withName
        self.greenPhase = greenPhase
        self.amberPhase = amberPhase
    }
    
    func start() {
        if self.phaseWorkItem != nil {
            self.phaseWorkItem?.cancel()
            self.phaseWorkItem = nil
        }
        isRunning = true
        self.subjectPhase.onNext(.running)
        self.subject.onNext(.green)
        let amberWorkItem = DispatchWorkItem { [weak self] in
            guard self != nil else {
                return
            }
            // we need to check for cancellations, because they don't actually cancel the task once it's running
            if (self?.isRunning)! && !(self?.phaseWorkItem?.isCancelled)! {
                self?.subject.onNext(.amber)
                let redWorkItem = DispatchWorkItem { [weak self] in
                    guard self != nil else {
                        return
                    }
                    // we need to check for cancellations, because they don't actually cancel the task once it's running
                    if (self?.isRunning)! && !(self?.phaseWorkItem?.isCancelled)! {
                        self?.subject.onNext(.red)
                        self?.subjectPhase.onNext(.completed)
                    }
                }
                self?.phaseWorkItem = redWorkItem
                DispatchQueue.global(qos: .default).asyncAfter(deadline: .now() +  (self?.amberPhase)!, execute: redWorkItem)
            }
        }
        self.phaseWorkItem = amberWorkItem
        DispatchQueue.global(qos: .default).asyncAfter(deadline: .now() +  self.greenPhase, execute: amberWorkItem)

        return
    }
    
    func stop() {
        if phaseWorkItem != nil {
            phaseWorkItem?.cancel()
            phaseWorkItem = nil
        }
        isRunning = false
        subject.onNext(TrafficLightState.red)
        self.subjectPhase.onNext(.stopped)
    }
    
    func asLightStateObservable() -> Observable<TrafficLightState> {
        let observable : Observable<TrafficLightState> = subject
        return observable;
    }
    
    func asPhaseStateObservable() -> Observable<TrafficLightPhaseState> {
        let observable : Observable<TrafficLightPhaseState> = subjectPhase
        return observable;
    }
    
    // Equatable
    final class func == (lhs: TrafficLightPhase, rhs: TrafficLightPhase) -> Bool {
        return lhs.name == rhs.name
    }
}
