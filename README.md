## Summary

A single­view iOS app that simulates a set of traffic lights at an intersection.
 
## Requirements

The traffic lights should be arranged North­South and East­West like a compass. And there should be a button to start/stop the animation. It’s completely up to you how the layout is done, feel free to use code or Interface Builder, whatever you like.
 
When switching from green to red, the yellow light must be displayed for 5 seconds prior to it switching to red. The lights will change automatically every 30 seconds.
 
## Design Philosophy

High level design patterns used include reactive programming (using RxSwift and RxCocoa) and model-view-viewmodel. Use of MVVM was somewhat contrived, and possibly not necessary for such as simple app, but wanted to show basic constructs and my preferred design approaches.
 
I chose not to use dependency injection (I like Swinject) and prototype based programming, as this wasn’t really required either, but with further time, that would be one optimisation I would have applied.
